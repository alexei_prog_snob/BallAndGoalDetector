# Ball And Goal Detector
My python code for ball and goal detector for robot vision.

This code is for Raspberry Pi camera Raspicam.
I detect a pink ball by color and shape and use camera focal length to detect the distance from the ball. 
For knowing the distance you have to know the ball physical radius.
Same for the goal detections.
