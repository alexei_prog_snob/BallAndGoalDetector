import os
import cv2
import math
import numpy as np
import thread
import argparse
import imutils
from collections import deque


def distance_to_camera(knownWidth, focalLength, perWidth):
	# compute and return the distance from the maker to the camera
	return (knownWidth * focalLength) / perWidth

def BallDect(frame):
	
	focalLength = 779.0

	redLower = (150, 84, 130)
	redUpper = (358,256,255)

	KNOWN_WIDTH = 3.5

	inches=0
	r=0
	blurred = cv2.GaussianBlur(frame,(11,11),0)
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	mask = cv2.inRange(hsv, redLower, redUpper)
	mask = cv2.erode(mask, None, iterations=4)
	mask = cv2.dilate(mask, None, iterations=4)
	circles = cv2.HoughCircles(mask,cv2.cv.CV_HOUGH_GRADIENT,1,75,param1=20,param2=3,minRadius=0,maxRadius=100)
	found=False
	if circles is not None:
		cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
		center = None
			# only proceed if at least one contour was found
		if len(cnts) > 0:
			# find the largest contour in the mask, then use
			# it to compute the minimum enclosing circle and
			# centroid
			c = max(cnts, key=cv2.contourArea)
			((x, y), radius) = cv2.minEnclosingCircle(c)
			M = cv2.moments(c)
			center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
	 
			# only proceed if the radius meets a minimum size
			if radius > 10:
				r=int(radius)
				inches = distance_to_camera(KNOWN_WIDTH, focalLength,2*r)
	return [inches,r]
	
def GateDect(frame):
	focalLength = 775.0

	KNOWN_WIDTH = 35.5
	 
	redLower = (20, 100, 100)
	redUpper = (43,255,255)   
	blurred = cv2.GaussianBlur(frame,(11,11),0)
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	mask = cv2.inRange(hsv, redLower, redUpper)
	mask = cv2.erode(mask, None, iterations=4)
	mask = cv2.dilate(mask, None, iterations=4)
	inches=0
	w=0
	contours,hierarchy = cv2.findContours(mask,1, 2)

	if len(contours) > 0:
		cnt = contours[0]
		area = cv2.contourArea(cnt)
		if area >1000:

			x,y,w,h = cv2.boundingRect(cnt) 
			inches = distance_to_camera(KNOWN_WIDTH, focalLength, w) 
	return [inches,w]
