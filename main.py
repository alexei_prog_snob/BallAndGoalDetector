## Python Libraries 
import os
import cv2
import math
import numpy as np
import thread
import argparse
import imutils
from collections import deque
from picamera.array import PiRGBArray
from picamera import PiCamera
import time

## Our Function 
import ImageProc
import RobotCont
import GyroTest
RobotCont.Standing()

#####
camera=PiCamera()
camera.resolution=(600,600)
camera.framerate=32
rawCapture=PiRGBArray(camera,size=())
time.sleep(5)

for frame in camera.capture_continuous(rawCapture,format="bgr", use_video_port=True):
	
	image=frame.array	

	(inches_ball,c_radius)=ImageProc.BallDect(image)
	(inches,c_width)=ImageProc.GateDect(image)

	rawCapture.truncate(0)

